<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});
Route::group(['prefix'=>'home'],function(){
	
	//IndexController
	Route::get('playground','Home\IndexController@index');
	Route::get('regist','Home\IndexController@regist');
	Route::post('getRegist','Home\IndexController@getRegist');
	Route::get('login','Home\IndexController@login');
	Route::post('getLogin','Home\IndexController@getLogin');
	Route::get('logout','Home\IndexController@logout');
	//ActionController
	Route::get('actionUpload','Home\ActionController@actionUpload');
	Route::post('contentUpload','Home\ActionController@contentUpload');
	Route::get('lookComment','Home\ActionController@lookComment');
	Route::post('setComment','Home\ActionController@setComment');
	Route::get('myAction','Home\ActionController@myAction');
	Route::any('good','Home\ActionController@good');
	Route::any('delete','Home\ActionController@delete');
	Route::any('anwser','Home\ActionController@anwser');
	Route::any('setAnwser','Home\ActionController@setAnwser');
});
