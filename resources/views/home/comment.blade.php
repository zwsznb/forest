<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		@include('/home.top')
		
		
		<div class="container">
		<div class="page-header">
  <h3><img src="{{$data->head}}" style="width: 100px;height: 100px;" class="img-circle">{{$data->username==Cache::get('username')?"我":$data->username}}<small>发布于:{{$data->Adate}}</small></h3>
</div>
		</div>
		
		
		
		<div class="row">
			<div class="col-md-6 col-lg-offset-1" style="margin-top: 20px;">
				<div class="row">
					@if($data->Aimg==null)
					@else
					<div class="col-md-3">
                       <img src="{{$data->Aimg}}" style="width: 100px;height: 80px;" class="img-thumbnail">
					</div>
					@endif
					
					
				</div>
			</div>
			<div class="col-md-4 col-md-offset-1">
			<div class="list-group" style="position: fixed;width: 300px;margin-top: 20px;">
  <a href="#" class="list-group-item list-group-item-success">
    Cras justo odio
  </a>
  <a href="#" class="list-group-item">回到顶部</a>
  <a href="#" class="list-group-item">回到顶部</a>
  <a href="#" class="list-group-item">回到顶部</a>
  <a href="#" class="list-group-item">回到顶部</a>
</div>
			
			</div>
			
		</div>
		
		<div class="row">
			<div class="col-md-6 col-lg-offset-1">
				<p>{{$data->content}}</p>
				<a href="#" class="aleft">赞<span class="badge">{{$data->good}}</span></a>
			</div>
		</div>
		
		
		<div class="row" style="margin-top: 20px;">
			<div class="col-md-6 col-lg-offset-1">
		<form action="/home/setComment?id={{$data->id}}" method="post">
			{{ csrf_field() }}

		<div class="form-group">
  	<label for="exampleInputPassword1">发表评论</label>
     <textarea class="form-control" rows="3" maxlength="800" name="content" placeholder="内容限800字以内"></textarea>
  </div>
  <button type="submit">提交</button>
		</form>
  </div>
		</div>
		
		
		
		<div class="row">
			<div class="col-md-6 col-lg-offset-1">
				<div class="page-header">
  <h4>评论 ({{$data->comment}})</h4>
</div>
			</div>
		</div>
		
		@if($action->count()==0)
		<div class="row">
		<div class="col-md-6 col-lg-offset-1">
		<h4>暂无用户评论</h4>
		</div></div>
		@else
		@foreach($action as $comment)
		<div class="row mtop">
			<div class="col-md-6 col-lg-offset-1">
				<div class="media">
  <div class="media-left media-middle">
    <a href="#">
      <img class="media-object img-circle" src="{{$comment->head}}" style="width: 50px;height: 50px;">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">{{$comment->username==Cache::get('username')?"我":$comment->username}}
    @if($comment->to_username!=null&& $comment->to_username!='')
        &nbsp;回复 &nbsp; <span>@</span>{{$comment->to_username==Cache::get('username')?"我":$comment->to_username}}
      @endif
    </h4>
    {{$comment->content}}<br/>
    <a href="/home/anwser?from_id={{Cache::get('uid')}}&&to_uid={{$comment->from_uid}}&&Aid={{$comment->Aid}}" class="aleft">回复</a>
	              <!--<a href="#" class="aleft">评论<span class="badge">42</span></a>-->
  </div>
  
</div>
				</div>
		</div>
		<hr/>
		@endforeach
		@endif
		<!--<div class="row mtop">
			<div class="col-md-6 col-lg-offset-1">
				<div class="media">
  <div class="media-left media-middle">
    <a href="#">
      <img class="media-object img-circle" src="img/uugai.com-1557487755192.png" style="width: 100px;height: 100px;">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">用户1</h4>
    如果你想忘掉以前的记忆,就可以对着树洞倾诉,再用土把它填上,这样你的秘密就被永远地保留在树洞里了如果你想忘掉以前的记忆,就可以对着树洞倾诉,再用土把它填上,这样你的秘密就被永远地保留在树洞里了如果你想忘掉以前的记忆,就可以对着树洞倾诉,再用土把它填上,这样你的秘密就被永远地保留在树洞里了如果你想忘掉以前的记忆,就可以对着树洞倾诉,再用土把它填上,这样你的秘密就被永远地保留在树洞里了如果你想忘掉以前的记忆,就可以对着树洞倾诉,再用土把它填上,这样你的秘密就被永远地保留在树洞里了<br/>
    <a href="#" class="aleft">回复<span class="badge">42</span></a>
	              <a href="#" class="aleft">评论<span class="badge">42</span></a>
  </div>
  
</div>
				</div>
		</div>-->
		
		
		</div>
		
		
	</body>
</html>
