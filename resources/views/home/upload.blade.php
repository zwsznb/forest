<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
	
	@include('home.top')
	
	
		<div class="container">
		<div class="page-header">
  <h1>大声呐喊吧</h1>
</div>
		</div>
		
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
		<form action="/home/contentUpload" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}

  <div class="form-group">
  	<label for="exampleInputPassword1">Content</label>
     <textarea class="form-control" rows="3" maxlength="800" name="content" placeholder="内容限800字以内"></textarea>
  </div>
   <div class="form-group">
  	<select name="visable" class="form-control">
  <option value="0">仅自己可见</option>
  <option value="1">任何人可见</option>
</select>
  	</div>
  <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input name="file" type="file" id="exampleInputFile">
  </div>
 

  <button type="submit" class="btn btn-default">提交</button>
</form>
		</div>
		</div>
		
	</body>
</html>
