<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		
		@include('/home.top')
		
		<div class="container">
		<div class="page-header">
  <h1>欢迎来到广场 <small>Welcome to playground</small></h1>
</div>
		</div>
		
		
@if($data->count()==0)
<div class="col-md-6 col-lg-offset-1" style="margin-top: 20px;">
  <h4>暂无动态</h4>
</div>
@else
	@foreach ($data as $action)
		
			<div class="col-md-6 col-lg-offset-1" style="margin-top: 20px;">
				<div class="panel panel-default">
	              <div class="panel-body">
	              	<img src="{{$action->head}}" class="img-circle" style="width: 40px;height: 40px;">
	              		{{$action->username==Cache::get('username')?"我":$action->username}}
	              </div>
	              <div class="panel-footer">
	              	@if($action->Aimg==null)
	              	
	                @else
	                <div class="row">
	              		<div class="col-md-3">
	                  <img src="{{$action->Aimg}}" style="width: 100px;height: 80px;"/>
	                </div>
	                </div>
	                @endif
	              	{{$action->content}}
	              	
	              </div>
	              <span class="aleft" onclick="good({{$action->id}})" style="cursor: pointer;">赞<span class="badge" id="good{{$action->id}}">{{$action->good}}</span></span>
	              <a href="/home/lookComment?id={{$action->id}}" class="aleft">评论<span class="badge">{{$action->comment}}</span></a>
	              <span style="font-size: 10px;color: darkgray;float: right;margin-right: 25px;">{{$action->Adate}}</span>
                </div>
			</div>
			@endforeach

		<div class="col-md-4 col-md-offset-1">
			<div class="list-group" style=" top:50%; position: fixed;width: 300px;margin-top: 20px;">
  <a href="#" class="list-group-item list-group-item-success">
    Cras justo odio
  </a>
  <a href="#" class="list-group-item">回到顶部</a>
  <a href="#" class="list-group-item">回到顶部</a>
  <a href="#" class="list-group-item">回到顶部</a>
  <a href="#" class="list-group-item">回到顶部</a>
</div>
			
			</div>
		
		
		
		<div class="container">
			
    @foreach ($data as $action)
        {{ $action->name }}
    @endforeach
     </div>
<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
    {{ $data->links() }}

</div>
		</div>
		</div>
		
		@endif
		
		
		<!--<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
		<nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
</div>
		</div>
		</div>-->
		
		
		
		</div>
	</body>
	<!--<script src="Forest/public/js/jquery-3.2.1.min.js" type="text/javascript"/>-->
	<script type="text/javascript">
		function good(id){
			//alert(id);
			//alert($('span #good').text());
			$.post('/home/good',{'_token':'{{csrf_token()}}','id':id},function(data,status){
				var i=id;
				if(data=="成功"){
				$('span #good'+i).text(parseInt($('span #good'+i).text())+1);
				}else{
					alert(data);
				}
			})
		}
	</script>
</html>
