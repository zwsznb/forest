<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $table="ft_action";
    //定义主键
    protected $primaryKey="id";
    //定义禁止操作时间
    public $timestamps=false;
    //设置允许操作字段
    protected $fillable=['id','Uid','content','good','comment','Adate','Aimg','visable'];
}
