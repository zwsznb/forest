<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table="comment";
    //定义主键
    protected $primaryKey="id";
    //定义禁止操作时间
    public $timestamps=false;
    //设置允许操作字段
    protected $fillable=['id','Aid','content','from_uid','to_uid'];
}
