<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use Cache;
use App\Model\Action;
class IndexController extends Controller
{   
	//广场页
    public function index(){
    	$data=Action::join('ft_user', 'ft_action.Uid', '=', 'ft_user.id')->where('visable','1')
->select('Uid','ft_action.id','content','good','comment','Adate','Aimg',"username",'head')->paginate(5);
    	return view('home.playground',compact('data'));
    }
    //显示注册页
    public function regist(){
    	return view('home.regist');
    }
    //获取注册信息
    public function getRegist(Request $request){
    	if($request->isMethod('post')){
    		$this->validate($request, [
                 'username' => 'required|max:20|min:2',
                 'email' => 'required',
                 'password'=>'required|min:8|max:20',
                 'head'=>'image'
            ]);
            $data=$request->all();
            $data['lastloginip']=$request->getClientIp();
            $data['password']=md5($request->password);
//          $date=date_create();
            $data['lastlogintime']=date("Y-m-d");
            if($request->hasFile('head')&&$request->file('head')->isValid() ){      	$head=md5(time().rand(100000,999999)).'.'.$request->file('head')->getClientOriginalExtension();
            	$file = $request->file('head')->move('./upload',$head);
            	$data['head']="/upload/".$head;
            }
            if(isset($data['head'])){
            	$result=User::insertGetId(['head'=>$data['head'],'username'=>$data['username'],'password'=>$data['password'],'email'=>$data['email'],'lastloginip'=>$data['lastloginip'],'lastlogintime'=>$data['lastlogintime']]);
            }else{
            	$result=User::insertGetId(['username'=>$data['username'],'password'=>$data['password'],'email'=>$data['email'],'lastloginip'=>$data['lastloginip'],'lastlogintime'=>$data['lastlogintime']]);
            }
            
            Cache::forever('username', $data['username']);
            Cache::forever('uid', $result);
            if(isset($data['head'])){
            Cache::forever('head', $data['head']);
            }else{
            	Cache::forever('head', '/img/uugai.com-1557487755192.png');
            }

            if($result>0){
            	return $this->index();
            }else{
            	return view('home.regist');
            }
            
    	}else{
    		return view('home.regist');
    	}
    }
    //登录
    public function login(){
    	return view('home.login');
    }
    public function getLogin(Request $request){
    	if($request->isMethod('post')){
    		$this->validate($request, [
                 'username' => 'required|max:20|min:2',
                 'password'=>'required|min:8|max:20'
            ]);
    	$username=$request->username;
    	$password=md5($request->password);
    	$user = User::where([['username',$username],['password',$password]])->select('id', 'username','head')->get()->first();
    	if($user!=null && $user!=''){
    			Cache::forever('username', $user->username);
            Cache::forever('uid', $user->id);
            Cache::forever('head',$user->head);
    		
    		return $this->index();
    	}else{
    		echo "<script>alert('登录失败');</script>";
    		return view('home.login');
    	}
     }else{
     	return view('home.login');
     }
     }
     public function logout(){
     	Cache::forget('username');
     	Cache::forget('id');
     	Cache::forget('head');
     	return view('home.index');
     }
    
}
