<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use Cache;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Action;
use App\Model\Comment;
use Input;
use App\Model\User;
class ActionController extends Controller
{
    //动态上传页面
     public function actionUpload(){
     	if(Cache::get('username')!=null){
     		return view('home.upload');
     	}else{
     		echo "<script>alert('请登录')</script>";
     		return view('home.login');
     	}
     	
     }
     //动态上传
     public function contentUpload(Request $request){
     	if($request->isMethod('post')){
    		$this->validate($request, [
                 'content' => 'required|max:800|min:1',
                 'file'=>'image'
            ]);
            $data=$request->all();
            $date=date_create();
            $data['Adate']=date_timestamp_get($date);
            $data['Uid']=Cache::get('uid');
            if($request->hasFile('file')&&$request->file('file')->isValid() ){      	$img=md5(time().rand(100000,999999)).'.'.$request->file('file')->getClientOriginalExtension();
            	$file = $request->file('file')->move('./action',$img);
            	$data['Aimg']="/action/".$img;
            }
            if(isset($data['file'])){
            	$result=Action::create($data);
            }else{
            	$result=Action::insert(['Uid'=>$data['Uid'],'content'=>$data['content'],'Adate'=>$data['Adate'],'visable'=>$data['visable']]);
            }
            return redirect()->action('Home\IndexController@index');

       }else{
       	   return view('home.actionUpload');
       }
     }
     //查看评论
     public function lookComment(){
     	$data=Action::join('ft_user', 'ft_action.Uid', '=', 'ft_user.id')->where('ft_action.id',Input::get('id'))
->select('Uid','ft_action.id','content','good','comment','Adate','Aimg',"username",'head')->get()->first();
       // $comment=Comment::where('Aid',$request->id)->get();
       $action = Action::
            join('comment', 'ft_action.id', '=', 'comment.Aid')
            ->join('ft_user', 'ft_user.id', '=', 'comment.from_uid')->where('ft_action.id',Input::get('id'))
            ->select('username', 'from_uid', 'head','comment.content','to_uid','Aid')
            ->get();
            //获取回复人
            foreach($action as $value){
              	  if($value->to_uid!=null&&$value->to_uid!=''){
              	  	$data1=User::where('id',$value->to_uid)->select('username')->get()->first();
              	  	$value->to_username=$data1->username;
              	  }
              }
     	return view('home.comment',compact('data','action'));
     }
     //发表评论
     public function setComment(){
 $result=Comment::insert(['Aid'=>Input::get('id'),'content'=>Input::get('content'),'from_uid'=>Cache::get('uid')]);
       if($result>0){
       	   //$data=Action::where('id',Input::get('id'))->select('comment')->get()->first();
       	   Action::where('id',Input::get('id'))->increment('comment',1);
       	   echo "<script>alert('评论成功');</script>";
       }else{
       	  echo "<script>alert('评论失败');</script>";
       }
       return redirect()->action('Home\ActionController@lookComment', ['id'=>Input::get('id')]);
       
     }
     //我的动态
     public function myAction(){
     	if(Cache::get('username')!=null){
     	$id=Cache::get('uid');
     	$data=Action::join('ft_user', 'ft_action.Uid', '=', 'ft_user.id')->where('ft_action.Uid',$id)
->select('Uid','ft_action.id','content','good','comment','Adate','Aimg',"username",'head')->get();
     	return view('home.my',compact('data'));
     	}else{
     		echo "<script>alert('请登录')</script>";
     		return view('home.login');
     	}
     }
     //用户点赞
     public function good(Request $request){
     	$result=Action::where("id",$request->id)->increment('good',1);
     	if($result){
     		echo "成功";
     	}else{
     		echo "失败";
     	}
     }
     //删除动态
     public function delete(){

     }
     //显示回复动态页面
     public function anwser(Request $request){
     	return view('home.anwser');
     }
     //回复动态
     public function setAnwser(){
     	$data=Comment::insert(
    ['content' => Input::get('content'), 'from_uid' => Input::get('from_id'),'to_uid'=>Input::get('to_uid'),'Aid'=>Input::get('Aid')]
);
        if($data){
        	$result=Action::where("id",Input::get('Aid'))->increment('comment',1);
        	return redirect()->action('Home\ActionController@lookComment', ['id'=>Input::get('Aid')]);
        }else{
        	return "回复失败";
        }
     }
  }
