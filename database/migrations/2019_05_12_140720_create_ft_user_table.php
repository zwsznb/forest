<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFtUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ft_user', function (Blueprint $table) {
            $table->increments('id')->notNull();
            $table->string('username',20)->notNull()->default('')->unique();
            $table->string('password',40)->notNull()->default('');
            $table->string('email',20)->notNull()->default('');
            $table->string('lastloginip')->notNull()->default(0);
            $table->string('lastlogintime')->notNull()->default(0);
            $table->string('head',100)->nullable()->default('/img/uugai.com-1557487755192.png	');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('ft_user');
    }
}
