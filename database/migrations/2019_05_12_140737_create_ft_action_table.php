<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFtActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ft_action', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Uid')->notNull();
            $table->foreign('Uid')->references('id')->on('ft_user');
            $table->string('content',800)->notNull();
            $table->integer('good')->notNull()->default(0);
            $table->integer('comment')->notNull()->default(0);
            $table->date('Adate')->notNull();
            $table->string('Aimg',80)->nullable()->default('');
            $table->char('visable',1)->notNull()->default('0');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('ft_action');
    }
}
