<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Aid')->notNull();
            $table->foreign('Aid')->references('id')->on('ft_action');
            $table->string('content',800)->notNull();
            $table->integer('from_uid')->notNull();
            $table->foreign('from_uid')->references('id')->on('ft_user');
            $table->integer('to_uid')->nullable();
            $table->foreign('to_id')->references('id')->on('ft_user');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment');
    }
}
